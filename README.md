Math_Matrix
===========

Package to represent matrices and matrix operations.  
Matrices are represented as 2 dimensional arrays of numbers. 

The classes in this package define methods for matrix objects, as well as 
static methods to read, write and manipulate matrices, including methods 
to solve systems  of linear equations (with and without iterative error 
correction).

Requires the Math_Vector package.

In order to run the unit tests you will need PHPUnit version <= 0.6.2.
(Yes, I know it is an old version of PHPUnit)

This is old code I wrote for [PEAR](http://pear.php.net). 
Originally it was tracked in CVS, then it was moved to SVN, 
and now I am importing it to git :-)

-- Jesus M. Castagnetto
